#pragma once

#include <vector>
#include <stdexcept>
#include <type_traits>

template<typename T = double, typename U = double>
using mul_t = decltype(std::declval<T>() * std::declval<U>());

template<typename T = double, typename U = double>
using quot_t = decltype(std::declval<T>() / std::declval<U>());

template<typename T = double, typename U = double>
using sum_t = decltype(std::declval<T>() + std::declval<U>());

template<typename T = double, typename U = double>
using sub_t = decltype(std::declval<T>() - std::declval<U>());

template<typename T = double, typename U = double>
inline auto dot_product(
    const std::vector<T>& a, const std::vector<U>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");

    mul_t<T, U> product{ 0 };
    for (size_t i = 0; i < a.size(); i++)
        product += a[i] * b[i];

    return product;
}

// Element-wise multiplication of two vectors.
template<typename T = double, typename U = double>
inline auto operator*(
    const std::vector<T>& a, const std::vector<U>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");

    std::vector< mul_t<T, U> > vec;
    vec.reserve(a.size());
    for (int i = 0; i < a.size(); i++)
        vec.push_back(a[i] * b[i]);

    return vec;
}

template<typename T = double, typename U = double>
inline auto operator*(
    const T alpha, const std::vector<U>& a)
{
    std::vector< mul_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(alpha * a[i]);
    return vec;
}

template<typename T = double, typename U = double>
inline auto operator*(
    const std::vector<T>& a, const U alpha)
{
    std::vector< mul_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(a[i] * alpha);
    return vec;
}

// Element-wise division of two vectors.
template<typename T = double, typename U = double>
inline auto operator/(
    const std::vector<T>& a, const std::vector<T>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");

    std::vector< quot_t<T, U> > vec;
    vec.reserve(a.size());
    for (int i = 0; i < a.size(); i++)
        vec.push_back(a[i] / b[i]);

    return vec;
}

template<typename T = double, typename U = double>
inline auto operator/(
    const std::vector<T>& a, const U alpha)
{
    std::vector< quot_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(a[i] / alpha);
    return vec;
}

template<typename T = double, typename U = double>
inline auto operator+(
    const std::vector<T>& a, const std::vector<U>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");

    std::vector< sum_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(a[i] + b[i]);

    return vec;
}

template<typename T = double, typename U = double>
inline auto operator+(
    const std::vector<T>& a, const U alpha)
{
    std::vector< sum_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(a[i] + alpha);
    return vec;
}

template<typename T = double, typename U = double>
inline auto operator-(
    const std::vector<T>& a, const std::vector<U>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");

    std::vector< sub_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(a[i] - b[i]);

    return vec;
}

template<typename T = double, typename U = double>
inline auto operator-(
    const std::vector<T>& a, const U alpha)
{
    std::vector< sub_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(a[i] - alpha);
    return vec;
}

template<typename T = double, typename U = double>
inline auto operator-(
    const T alpha, const std::vector<U>& a)
{
    std::vector< sub_t<T, U> > vec;
    vec.reserve(a.size());
    for (size_t i = 0; i < a.size(); i++)
        vec.push_back(alpha - a[i]);
    return vec;
}

template<typename T = double, typename U = double>
inline std::vector<T>& operator+=(
    std::vector<T>& a, const std::vector<U>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");
   
    for (size_t i = 0; i < a.size(); i++)
        a[i] += b[i];

    return a;
}

template<typename T = double, typename U = double>
inline std::vector<T>& operator-=(
    std::vector<T>& a, const std::vector<U>& b)
{
    if (a.size() != b.size())
        throw std::invalid_argument(__FUNCTION__": vector sizes don't match");

    for (size_t i = 0; i < a.size(); i++)
        a[i] -= b[i];

    return a;
}

template<typename T = double>
inline std::ostream& operator<<(
    std::ostream& os, const std::vector<T>& a)
{
    os << "{ ";
    for (const T& x : a)
        os << x << " " ;
    os << "}";
    return os;
}
