#include <ctime>
#include <iostream>
#include <iomanip>
#include "Matrix.h"
#include "Vector.h"
#include "logistic_regression.h"
#include "linear_regression.h"
#include "Normalization.h"
#include "NeuralNetwork.h"

void init()
{
    std::cout << std::fixed << std::setprecision(3);
    srand(static_cast<unsigned int>(time(NULL)));
}

void test_neural_network()
{
    const int nInputs = 2;
    const int nHiddenLayers = 2;
    const int nHiddenNeurons = 2;
    const int nOutputs = 1;

    const int learning_passes = 100;
    const double alpha = 1.0;
    const double lambda = 0.0;

    Matrix<> x(4, nInputs);
    x[0] = { 0.0, 0.0 };
    x[1] = { 0.0, 1.0 };
    x[2] = { 1.0, 0.0 };
    x[3] = { 1.0, 1.0 };
    std::vector<double> y =
    {
        0.0,
        1.0,
        1.0,
        0.0
    };

    NeuralNetwork network(
        nHiddenLayers,
        nInputs,
        nHiddenNeurons,
        nOutputs);

    do
    {
        for (int i = 0; i < x.rows(); i++)
        {
            auto out = network.forward_propagation(x[i]);
            network.back_propagation(std::vector<double>{y[i]}, lambda, y.size());

            //std::cout << "==================================" << std::endl;
            //std::cout << network << std::endl;

            std::cout << "y = " << y[i] << " out = " << out[1] << std::endl;
            //std::cout << "cost = " << network.cost_function(std::vector<double>{y[i]}) << std::endl;
        }
        network.ApplyDeltaTheta(alpha);
    } while (std::cin.get() == '\n');
}

double foo(double x)
{
    return (x + 3.0) * (x - 3.0);
}

void test_linear_regression()
{
    const double alpha = 0.01;
    const double lambda = 0.0;
    const int nIterations = 10000;

    const int N = 3;
    const int m = 50;
    const double x_begin = -100.0;
    const double x_end = 100.0;
    const double x_step = (x_end - x_begin) / m;

    Matrix<double> x(m, N);
    std::vector<double> y(m, 0.0);
    for (int i = 0; i < m; i++)
    {
        auto x1 = x_begin + i * x_step;
        x.row(i) = { 1.0, x1, x1 * x1 };
        y[i] = foo(x1);
    }

    auto thetas = linear_regression::regression(
        x,
        y,
        alpha,
        nIterations,
        lambda);

    std::cout << thetas << std::endl;
}

void test_linear_regression_normalization()
{
    const double alpha = 0.01;
    const double lambda = 0.0;
    const int nIterations = 10000;

    const int N = 3;
    const int m = 50;
    const double x_begin = -100.0;
    const double x_end = 100.0;
    const double x_step = (x_end - x_begin) / m;

    Matrix<double> x(m, N);
    std::vector<double> y(m, 0.0);
    for (int i = 0; i < m; i++)
    {
        auto x1 = x_begin + i * x_step;
        x.row(i) = { 1.0, x1, x1 * x1 };
        y[i] = foo(x1);
    }

    auto xnorm_meanx_rangex = ::normalize(x);

    auto thetas = linear_regression::regression(
        std::get<0>(xnorm_meanx_rangex),
        y,
        alpha,
        nIterations,
        lambda);

    thetas = ::denormalize(
        thetas,
        std::get<1>(xnorm_meanx_rangex),
        std::get<2>(xnorm_meanx_rangex));

    std::cout << thetas << std::endl;
}

int main()
{
    init();
    test_linear_regression();
    test_linear_regression_normalization();
    return 0;
}
