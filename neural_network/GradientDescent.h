#pragma once

#include <vector>
#include <functional>

template<typename T = double>
inline std::vector<T>& gradient_descent(
    std::function< std::vector<T>(std::vector<T>&) > gradient,
    std::vector<T>& theta,
    double alpha,
    int nIterations)
{
    for (int j = 0; j < nIterations; j++)
        theta = theta - alpha * gradient(theta);

    return theta;
}
