#pragma once

#include <iostream>
#include <vector>
#include "Vector.h"

template <typename T = double>
class Matrix
{
private:
	// container for rows of the matrix
	std::vector< std::vector<T> > internal_container;

public:
	Matrix(
		int rows,
		int columns) :

		internal_container(rows, std::vector<T>(columns, T(0)))
	{}

	T at(int row, int col) const { return internal_container.at(row).at(col); }
	T& at(int row, int col) { return internal_container.at(row).at(col); }

	int rows() const { return internal_container.size(); }
	int cols() const { return std::size(internal_container[0]); }

	// returns i-th row of matrix
	std::vector<T>& operator[](int i) { return internal_container[i]; }
	const std::vector<T>& operator[](int i) const { return internal_container[i]; }

	// returns i-th row of matrix
	std::vector<T>& row(int i) { return internal_container[i]; }
	const std::vector<T>& row(int i) const { return internal_container[i]; }

	// returns a copy of i-th column of matrix
	const std::vector<T> column(int j) const
	{
		 std::vector<T> vec;
		 vec.reserve(rows());
		 for (int i = 0; i < rows(); i++)
			 vec.push_back(internal_container[i][j]);
		 return vec;
	}
};

template <typename T>
inline std::vector<T> operator*(
	const Matrix<T>& a,
	const std::vector<T>& y)
{
	std::vector<T> b;
	b.reserve(a.rows());
	for (int i = 0; i < a.rows(); i++)
		b.push_back(dot_product(a[i], y));
	return b;
}

template <typename T>
inline std::ostream& operator<<(
	std::ostream& os,
	const Matrix<T>& x)
{
	for(int i=0; i < x.rows(); i++)
		os << x[i] << std::endl;
	return os;
}
