#pragma once

#include <vector>
#include <utility>
#include <stdexcept>
#include <cmath>

#include "Matrix.h"
#include "GradientDescent.h"

namespace linear_regression
{
    template<typename T = double>
    inline auto hypothesys(
        const std::vector<T>& theta,
        const std::vector<T>& x)
    {
        return dot_product(theta, x);
    }

    //  Cost function for n-dimension hypothesys
    template<typename T = double>
    inline auto cost_function(
        const std::vector<T>& theta,
        const Matrix<T>& x,
        const std::vector<T>& y,
        T lambda)
    {
        if (x.cols() != theta.size())
            throw std::invalid_argument(__FUNCTION__": x and theta don't match");
        if (x.rows() != y.size())
            throw std::invalid_argument(__FUNCTION__": x and y don't match");

        T sum{ 0 };
        for (int i = 0; i < x.rows(); i++)
        {
            auto h = hypothesys(theta, x[i]);
            sum += (h - y[i]) * (h - y[i]);
        }

        T reg{ 0 };
        for (int j = 1; j < theta.size(); j++)
            reg += theta[j] * theta[j];

        return (sum + lambda * reg) / (2 * x.rows());
    }

    template<typename T = double>
    inline std::vector<T> cost_function_gradient(
        const std::vector<T>& theta,
        const Matrix<T>& x,
        const std::vector<T>& y,
        T lambda)
    {
        if (x.cols() != theta.size())
            throw std::invalid_argument(__FUNCTION__": x and theta don't match");
        if (x.rows() != y.size())
            throw std::invalid_argument(__FUNCTION__": x and y don't match");

        std::vector<T> error;
        error.reserve(y.size());
        for (int i = 0; i < x.rows(); i++)
            error.push_back(hypothesys(theta, x[i]) - y[i]);

        std::vector<T> vec(theta.size(), T(0));
        for (int j = 0; j < x.cols(); j++)
        {
            vec[j] += error[0] * x[0][j];
            for (int i = 1; i < x.rows(); i++)
                vec[j] += error[i] * x[i][j] + lambda * theta[j];
        }
        return vec / x.rows();
    }

    template<typename T = double>
    inline std::vector<T> regression(
        const Matrix<T>& x,
        const std::vector<T>& y,
        double alpha,
        int nIterations,
        double lambda)
    {
        if (x.rows() != y.size())
            throw std::invalid_argument(__FUNCTION__": x and y don't match");

        std::vector<T> theta(x.cols(), T(0));
        auto gradient = std::bind(cost_function_gradient<T>, std::placeholders::_1, std::cref(x), std::cref(y), lambda);
        return gradient_descent<T>(gradient, theta, alpha, nIterations);
    }
}
