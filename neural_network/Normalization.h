#pragma once

#include <algorithm>
#include <vector>
#include <tuple>
#include <numeric>

// Returns a normalized dataset [x]: normalized(x) => (x - mean(x)) / (max(x) - min(x))
// Returns tuple { normalized(x), mean(x), max(x) - min(x) }
template<typename T>
std::tuple< Matrix<T>, std::vector<T>, std::vector<T> > normalize(const Matrix<T>& x)
{
	std::vector<T> mean(x.cols(), T(0));
	std::vector<T> range(x.cols(), T(0));

	for (int j = 1; j < x.cols(); j++)
	{
		auto vec = x.column(j);
		mean[j] = std::accumulate(vec.begin(), vec.end(), T(0)) / vec.size();
		range[j] = *std::max_element(vec.begin(), vec.end()) - *std::min_element(vec.begin(), vec.end());
	}

	Matrix<T> x_normalized(x.rows(), x.cols());
	for (int i = 0; i < x.rows(); i++)
		x_normalized[i][0] = T{ 1 };

	for (int j = 1; j < x.cols(); j++)
	{
		for (int i = 0; i < x.rows(); i++)
			x_normalized[i][j] = (x[i][j] - mean[j]) / range[j];
	}

	return { x_normalized, mean, range };
}

// Returns fitting parameters (theta) for the initial (not normalized) dataset
template<typename T>
inline std::vector<T> denormalize(
	const std::vector<T>& theta_in,
	const std::vector<T>& mean,
	const std::vector<T>& range)
{
	std::vector<T> theta;
	theta.reserve(theta_in.size());
	theta.push_back(theta_in[0]);
	for (size_t i = 1; i < theta_in.size(); i++)
	{
		theta.push_back(theta_in[i] / range[i]);
		theta[0] -= (theta_in[i] * mean[i]) / range[i];
	}
	return theta;
}