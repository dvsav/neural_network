#pragma once

#include <cstdlib>
#include <iostream>

#include "Matrix.h"
#include "Vector.h"
#include "logistic_regression.h"
#include "linear_regression.h"
#include "Normalization.h"

class Neuron
{
private:
    std::vector<double> thetas;   // vector of thetas: Theta(0) ... Theta(N)
    std::vector<double> d_thetas; // deltas of thetas (calculated during back propagation)
    double a;                     // neuron's output
    double err;                   // neuron's error (calculated during back propagation)

public:
    // nThetas - the number of thetas: Theta(0) ... Theta(N)
    // where nThetas = N+1
    Neuron(int nThetas) :
        thetas(),
        d_thetas(nThetas, 0.0),
        a(0.0),
        err(0.0)
    {
        if (nThetas < 1)
            throw std::invalid_argument(__FUNCTION__": nThetas cannot be < 1");

        thetas.reserve(nThetas);
        for (int i = 0; i < nThetas; i++)
            thetas.push_back(rand() / static_cast<double>(RAND_MAX) * 2.0 - 1.0);
    }

    Neuron(Neuron&& rvalue) noexcept :
        thetas(std::move(rvalue.thetas)),
        d_thetas(std::move(rvalue.d_thetas)),
        a(rvalue.a),
        err(rvalue.err)
    { }

    Neuron(const Neuron&) = delete;
    Neuron operator=(const Neuron&) = delete;

    // Performs forward propagation.
    // x[0] must be equal to 1.
    double forward_propagate(const std::vector<double>& x)
    {
        return a = logistic_regression::sigmoid(dot_product(x, thetas));
    }

    // Returns the output of the neuron
    double& out() { return a; }
    // Returns the output of the neuron
    const double& out() const { return a; }

    // Returns the error of the neuron
    double& error() { return err; }
    // Returns the error of the neuron
    const double& error() const { return err; }

    // Returns delta for Theta(i).
    // i = 0...N
    double& DeltaTheta(int i) { return d_thetas[i]; }
    const double& DeltaTheta(int i) const { return d_thetas[i]; }

    // Applies deltas to the thetas.
    void ApplyDeltaTheta(double alpha)
    {
        for (int i = 0; i < nInputs(); i++)
        {
            Theta(i) -= alpha * DeltaTheta(i);
            DeltaTheta(i) = 0.0;
        }
        error() = 0.0;
    }

    // Returns Theta(i).
    // i = 0...N
    double& Theta(int i) { return thetas[i]; }
    const double& Theta(int i) const { return thetas[i]; }

    // Returns the vector of thetas.
    const std::vector<double>& Thetas() const { return thetas; }

    // Returns deltas of thetas.
    const std::vector<double>& DeltaThetas() const { return d_thetas; }

    // Returns the number of inputs in the neuron (including the bias unit)
    // N+1
    const int nInputs() const { return thetas.size(); }
};

inline std::ostream& operator<<(std::ostream& os, const Neuron& neuron)
{
    os << "Thetas = " << neuron.Thetas() << " out = " << neuron.out() << " error = " << neuron.error() << " d(theta) = " << neuron.DeltaThetas();
    return os;
}

class Layer
{
private:
    std::vector<Neuron> neurons;

public:
    // nInputs  - the number of inputs in each neuron. nInputs = 1 for the input layer.
    // nNeurons - the number neurons in the layer (excluding the bias unit).
    Layer(int nInputs, int nNeurons) :
        neurons()
    {
        if (nNeurons < 1)
            throw std::invalid_argument(__FUNCTION__": nNeurons cannot be < 1");

        neurons.reserve(nNeurons);
        for (int i = 0; i < nNeurons; i++)
            neurons.push_back(Neuron(nInputs));
    }

    Layer(Layer&& rvalue) noexcept :
        neurons(std::move(rvalue.neurons))
    { }

    Layer(const Layer&) = delete;
    Layer operator=(const Layer&) = delete;

    // Performs forward propagation.
    // x[0] must be equal to 1.
    void forward_propagate(const std::vector<double>& x)
    {
        for (auto& neuron : neurons)
            neuron.forward_propagate(x);
    }

    // Returns the vector of outputs for the layer
    // (including the 1.0 which is the bias unit).
    const std::vector<double> outs() const
    {
        std::vector<double> vec;
        vec.reserve(nNeurons() + 1);
        vec.push_back(1.0);
        for (int i = 0; i < nNeurons(); i++)
            vec.push_back(neurons[i].out());
        return vec;
    }

    // Returns the i-th neuron
    // i = 0...nNeurons()-1
    Neuron& Neurons(int i) { return neurons[i]; }
    const Neuron& Neurons(int i) const { return neurons[i]; }

    // Returns the number of inputs in a neuron of the layer.
    const int nInputs() const { return Neurons(0).nInputs(); }

    // Returns the number of neurons in the layer.
    const int nNeurons() const { return neurons.size(); }

    // Applies deltas to all neurons in layer.
    void ApplyDeltaTheta(double alpha)
    {
        for (auto& neuron : neurons)
            neuron.ApplyDeltaTheta(alpha);
    }
};

inline std::ostream& operator<<(std::ostream& os, const Layer& layer)
{
    for (int i = 0; i < layer.nNeurons(); i++)
        os << "    neuron[" << i << "] = " << layer.Neurons(i) << std::endl;
    return os;
}

class NeuralNetwork
{
private:
    // Neural network consists of:
    // the input layer (the number of neurons equals the number of inputs of the network);
    // zero or more hidden layers;
    // the output layer (the number of neurons equals the number of outputs of the network).
    std::vector<Layer> layers;

public:
    // nHiddenLayers  - the number of hidden layers (>=0)
    // nInputs        - the number of inputs (x) not including the bias (1)
    // nHiddenNeurons - the number of neurons in a hidden layer
    // nOutputs       - the number of neurons in the output layer
    NeuralNetwork(
        int nHiddenLayers,
        int nInputs,
        int nHiddenNeurons,
        int nOutputs)
    {
        if (nHiddenLayers < 0)
            throw std::invalid_argument(__FUNCTION__": nHiddenLayers cannot be < 0");

        layers.reserve(nHiddenLayers + 2);

        // input layer (#1)
        layers.push_back(Layer(1, nInputs));

        // hidden layers (#2...#N-1)
        if (nHiddenLayers > 0)
        {
            // 1st hidden layer
            layers.push_back(Layer(nInputs+1, nHiddenNeurons));
            // other hidden layers
            for (int i = 1; i < nHiddenLayers; i++)
                layers.push_back(Layer(nHiddenNeurons+1, nHiddenNeurons));
        }
        else
            nHiddenNeurons = nInputs;

        // output layer (#N)
        layers.push_back(Layer(nHiddenNeurons+1, nOutputs));
    }

    NeuralNetwork(NeuralNetwork&& rvalue) noexcept :
        layers(std::move(rvalue.layers))
    { }

    NeuralNetwork(const NeuralNetwork&) = delete;
    NeuralNetwork operator=(const NeuralNetwork&) = delete;

    // Returns the i-th layer where i = 1...N
    Layer& Layers(int i) { return layers[i-1]; }
    const Layer& Layers(int i) const { return layers[i-1]; }

    const size_t N() const { return layers.size(); }

    // Does one forward propagation pass.
    // Returns the outputs of the network (including redundant 1 as the bias unit).
    std::vector<double> forward_propagation(
        const std::vector<double>& x)
    {
        // input layer
        for (size_t i = 0; i < x.size(); i++)
            Layers(1).Neurons(i).out() = x[i];

        // hidden layers
        for (size_t i = 2; i <= N(); i++)
            Layers(i).forward_propagate(Layers(i - 1).outs());
        
        // output layer
        return Layers(N()).outs();
    }

    // Does one back propagation pass.
    void back_propagation(
        const std::vector<double>& y, // desired output
        double lambda, // regularization coeffitient
        int m) // the size of dataset
    {
        // error calculation for the output layer
        auto& output_layer = Layers(N());
        if (y.size() != output_layer.nNeurons())
            throw std::invalid_argument(__FUNCTION__": y size doesn't match to the number of neurons in the output layer");
        for (int i = 0; i < output_layer.nNeurons(); i++)
            output_layer.Neurons(i).error() = output_layer.Neurons(i).out() - y[i];

        // error calculation for the hidden layers
        for (int i_layer = N()-1; i_layer > 1; i_layer--)
        {
            auto& layer = Layers(i_layer);
            auto& next_layer = Layers(i_layer + 1);

            for (int i_neuron = 0; i_neuron < layer.nNeurons(); i_neuron++)
            {
                layer.Neurons(i_neuron).error() = 0.0;

                for (int j = 0; j < next_layer.nNeurons(); j++)
                    layer.Neurons(i_neuron).error() += next_layer.Neurons(j).error() * next_layer.Neurons(j).Theta(i_neuron+1);
            }

            for (int i_neuron = 0; i_neuron < layer.nNeurons(); i_neuron++)
            {
                layer.Neurons(i_neuron).error() *= layer.Neurons(i_neuron).out() * (1.0 - layer.Neurons(i_neuron).out());
            }
        }

        // accumulation of delta(Theta) for all layers
        for (size_t l_layer = 2; l_layer <= layers.size(); l_layer++)
        {
            auto& layer = Layers(l_layer);
            auto& prev_layer = Layers(l_layer-1);

            for (int i_neuron = 0; i_neuron < layer.nNeurons(); i_neuron++)
            {
                auto& neuron = layer.Neurons(i_neuron);

                // Theta[0]
                neuron.DeltaTheta(0) += neuron.error() / m;

                // Theta[1...N]
                for (int j_theta = 1; j_theta < neuron.nInputs(); j_theta++)
                {
                    neuron.DeltaTheta(j_theta) += prev_layer.Neurons(j_theta - 1).out() * neuron.error() / m
                                                  + lambda * neuron.Theta(j_theta);
                }
            }
        }
    }

    // Applies deltas to all of the neurons in the network.
    void ApplyDeltaTheta(double alpha)
    {
        for (auto& layer : layers)
            layer.ApplyDeltaTheta(alpha);
    }

    double cost_function(const std::vector<double>& y) const
    {
        double sum = 0.0;
        auto& output_layer = Layers(N());

        for (size_t i = 0; i < y.size(); i++)
        {
            auto h = output_layer.Neurons(i).out();
            sum += y[i] * log(h) + (1 - y[i]) * log(1 - h);
        }
        return -sum;
    }
};

inline std::ostream& operator<<(std::ostream& os, const NeuralNetwork& nn)
{
    os << "layer[1] = " << std::endl;
    os << "    out = " << nn.Layers(1).outs() << std::endl << std::endl;

    for (size_t i = 2; i <= nn.N(); i++)
        os << "layer[" << i << "] = " << std::endl << nn.Layers(i) << std::endl;
    return os;
}
